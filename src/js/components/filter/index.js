import * as $ from 'jquery'
import {initPlaceholdersNew, isMobile} from "../helpers";
import lozad from "lozad";

export function initFilterLanding() {
    if (!$('#filter_landing').length) return;

    //Значения для 'Вид из окна'
    //1:'во двор',
    //2:'ул. Ново-садовая',
    //3:'р. Волга',
    //4:'на обе стороны'

    if (isMobile()){
        $('body').css('overflow','auto');
    }

    let rooms = JSON.parse($('.filter-results').attr('data-rooms'));

    //Массив квартир
    let FLATS_DB = [];

    //Массив отфильтрованных квартир
    let FILTER_DB = [];

    let FLAT_ON_PAGE = 10;

    let page = 1;

//формируем свой массив квартир по умолчанию со всеми полями для сортировки
    function getFlatsArray() {
        $.each(rooms, function (key, val) {
            FLATS_DB.push({
                id: +val.id,
                fullprice: +val.fullprice,
                windowView: +val.windowView,
                rooms: +val.rooms,
                square: +val.square,
                floor: +val.floor,
                image: val.image,
                url:val.url
            });
        });
    }

    //выбор количества комнат
    function initLocationTabs() {
        $('#location .item').on('click', function () {

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }

            refreshFilterResult();
        })
    }

    function refreshFilterResult() {
        page = 1;

        //Получаем фильтрованный массив
        getFilteredArray();
        //Применяем сортировку
        sortFilteredArray();
        //Обновляем HTML
        //createFilterResultHTML();

        const searchBtn = $('#rooms_conter');

        if(FILTER_DB.length === 0){
            searchBtn.html('Квартир не найдено');
            searchBtn.addClass('no-rooms');
        }else{
            searchBtn.html(FILTER_DB.length + ' ' + declOfNum(FILTER_DB.length, ['квартира', 'квартиры', 'квартир']));
            searchBtn.removeClass('no-rooms');
        }

    }

    function setFilterParamsFromInputs() {
        let def_rooms = $('#rooms_default').val(),
            def_residental = $('#residential_default').val(),
            def_radius = $('#radius_default').val();

        let $filterBlock = $('.filter-block'),
            $roomsSelect = $filterBlock.find('#rooms'),
            $jkSelect = $filterBlock.find('#jk');

        if (def_rooms) {
            $roomsSelect.val(def_rooms);
            $roomsSelect.parents('.field').removeClass('empty');
            $('.jq-selectbox').trigger('change');
            $roomsSelect.trigger('refresh');
        }

        if (def_residental) {
            $jkSelect.val(def_residental);
            $jkSelect.parents('.field').removeClass('empty');
            $('.jq-selectbox').trigger('change');
            $jkSelect.trigger('refresh');
        }

        if (def_radius) {
            $('#location .item[data-location="' + def_radius + '"]').addClass('active')
        }
    }

    function createFilterResultHTML() {
        createFlatItemListHTML();
    }

//Фильтрация массива по активному фильтру
    function sortFilteredArray() {
        let $sortBlock = $('#sorting'),
            sortType = $sortBlock.val(),
            sortDirection = $sortBlock.attr('data-sort');

        if (sortType === 'price') {
            if (sortDirection === 'asc') {
                FILTER_DB.sort(compareFlatsPriceAsc);
            } else {
                FILTER_DB.sort(compareFlatsPriceDesc);
            }
        }

        if (sortType === 'square') {
            if (sortDirection === 'asc') {
                FILTER_DB.sort(compareFlatsSquareAsc);
            } else {
                FILTER_DB.sort(compareFlatsSquareDesc);
            }

        }
    }

    //"показать ещё"
    function createFlatItemListHTML() {
        let start = 0,
            end = page * FLAT_ON_PAGE;

        let resultHTML = '';

        for (let i = start; i < end; i++) {
            let item = FILTER_DB[i];

            if (item) resultHTML += getFlatItem(item);
        }

        if (!resultHTML) resultHTML = '<p>Ничего не найдено</p>';

        checkShowmoreButton(end);

        $('.filter-results').html(resultHTML);
    }

    function checkShowmoreButton(end) {
        if (FILTER_DB.length <= end) {
            $('.filter-showmore').hide();
        } else {
            $('.filter-showmore').show();
        }
    }


//Формирование HTML квартиры
    function getFlatItem(item) {
        let resultHTML = '';

        if (!item) return;
        let photo = item.image,
            url = item.url,
            square = item.square,
            rooms = item.rooms,
            floor = item.floor,
            price = number_format(item.fullprice, 0, '', ' ');

        resultHTML =
            `<a href="${url}" class="flat-item">
                <div class="flat-info">
                    <img class="image" src="${photo}" alt="">
                    <div class="rooms">${rooms}-комнатная</div>
                    <div class="square">${square} м²</div> 
                    <div class="floor">${floor} этаж</div>
                    <div class="price">${price} ₽</div>
                </div>
                 <div class="flat-info-mob">
                    <img class="image" src="${photo}" alt="">
                    <div class="text">
                        <div class="rooms">${rooms}-комнатная</div>
                        <div class="mob-row">
                            <div class="square">${square} м²</div> 
                            <div class="floor">${floor} этаж</div>
                        </div>
                        <div class="price">${price} ₽</div>
                    </div>
                </div>          
            </a>`;

        if (!resultHTML) return '';

        return resultHTML;

    }

//Получение отфильтрованных квартир
    function getFilteredArray() {
        //Получение данных для фильтра
        let filterObj = getFilterParams();

        //Фильтрация
        FILTER_DB = FLATS_DB.filter(function (item) {

            return (!filterObj.rooms.length || filterObj.rooms.includes(item.rooms)) &&
                (!filterObj.minPrice || filterObj.minPrice <= item.fullprice) &&
                (!filterObj.maxPrice || filterObj.maxPrice >= item.fullprice) &&
                (!filterObj.minFloor || filterObj.minFloor <= item.floor) &&
                (!filterObj.minFloor || filterObj.maxFloor >= item.floor) &&
                (!filterObj.minSquare || filterObj.minSquare <= item.square) &&
                (!filterObj.maxSquare || filterObj.maxSquare >= item.square) &&
                (!filterObj.windowView || item.windowView == filterObj.windowView)
        });

    }

    function getFilterParams() {

        let rooms = [],
            minPrice = +$('#min_price_val').val(),
            maxPrice = +$('#max_price_val').val(),
            minFloor = +$('#min_floor_val').val(),
            maxFloor = +$('#max_floor_val').val(),
            minSquare = +$('#min_square_val').val(),
            maxSquare = +$('#max_square_val').val(),
            windowView = +$('#window_view').val();

        //берём все выбранные "Количество комнат"
        $('#location').find('.item.active').each((_,chosenRoom) => {
            rooms.push($(chosenRoom).data('location'));
        });

        return {
            'rooms': rooms,
            'minPrice': minPrice,
            'maxPrice': maxPrice,
            'minFloor': minFloor,
            'maxFloor': maxFloor,
            'minSquare': minSquare,
            'maxSquare': maxSquare,
            'windowView': windowView
        };
    }

// сортировка
    function compareFlatsPriceAsc(a, b) {
        if (+a.fullprice > +b.fullprice) return 1;
        if (+a.fullprice < +b.fullprice) return -1;
    }

    function compareFlatsPriceDesc(a, b) {
        if (+a.fullprice < +b.fullprice) return 1;
        if (+a.fullprice > +b.fullprice) return -1;
    }

    function compareFlatsSquareAsc(a, b) {
        if (+a.square > +b.square) return 1;
        if (+a.square < +b.square) return -1;
    }

    function compareFlatsSquareDesc(a, b) {
        if (+a.square < +b.square) return 1;
        if (+a.square > +b.square) return -1;
    }


    $(document).ready(function () {
            //Получаем массив с квартирами
            getFlatsArray();

            setFilterParamsFromInputs();

            createFilterResultHTML();

            initPlaceholdersNew();

            $('select').styler({
                selectSmartPositioning: false,
                selectSearch: false,
                onSelectClosed: function () {
                    //Скроллим к началу...
                    // $("html, body").animate({scrollTop: $('.filter-results').offset().top}, "slow");
                }
            });

            //костыль на селекты для айфонов
           if (isMobile()){

               const $viewWindowSelect = $('#window_view-styler .jq-selectbox__dropdown');
               const $filterSelect =  $('#sorting-styler .jq-selectbox__dropdown');

               $viewWindowSelect.addClass('hide');
               $viewWindowSelect.css('display','block');


               $('#window_view-styler').on('click',(e) => {
                   const block = $(e.currentTarget);
                   $viewWindowSelect.css('display','block');

                  if( block.hasClass('jqselect')){
                      $viewWindowSelect.toggleClass('hide');
                  }
               });

               $filterSelect.addClass('hide');
               $filterSelect.css('display','block');


               $('#sorting-styler').on('click',(e) => {
                   const block = $(e.currentTarget);
                   $filterSelect.css('display','block');

                   if( block.hasClass('jqselect')){
                       $filterSelect.toggleClass('hide');
                   }
               });

           }

            //при изменении "Вид из окна" - обновляем фильтр
            $('#window_view').on('change',() => {
                refreshFilterResult();
            });

            $('#sorting').on('change',() => {
                sortFilteredArray();
                createFilterResultHTML();
            });

            $('#rooms_conter').on('click', function () {
                let href = $(this).attr('data-href')

                if (href) {
                    window.location.href = href;
                } else {
                    createFilterResultHTML();
                }
            })


            let newMinPriceForHtml, newMaxPriceForHtml,
             newMinFloorForHtml, newMaxFloorForHtml,
             newMinSquareForHtml, newMaxSquareForHtml

            //Задание range нужных значений


            function filterForRangeAfter(DB_FILTERED_FLATS) {

                newMinPriceForHtml = DB_FILTERED_FLATS[0]?.fullprice;
                newMaxPriceForHtml = DB_FILTERED_FLATS[0]?.fullprice;
                newMinFloorForHtml = DB_FILTERED_FLATS[0]?.floor;
                newMaxFloorForHtml = DB_FILTERED_FLATS[0]?.floor;
                newMinSquareForHtml = DB_FILTERED_FLATS[0]?.square;
                newMaxSquareForHtml = DB_FILTERED_FLATS[0]?.square;

                newMinPriceForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc < curr.fullprice ? acc : curr.fullprice)
                }, DB_FILTERED_FLATS[0]?.fullprice);

                newMaxPriceForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc > curr.fullprice ? acc : curr.fullprice)
                }, DB_FILTERED_FLATS[0]?.fullprice);

                newMinFloorForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc < curr.floor ? acc : curr.floor)
                }, DB_FILTERED_FLATS[0]?.floor);

                newMaxFloorForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc > curr.floor ? acc : curr.floor)
                }, DB_FILTERED_FLATS[0]?.floor);

                newMinSquareForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc < curr.square ? acc : curr.square)
                }, DB_FILTERED_FLATS[0]?.square);

                newMaxSquareForHtml = DB_FILTERED_FLATS.reduce((acc, curr) => {
                    return (acc > curr.square ? acc : curr.square)
                }, DB_FILTERED_FLATS[0]?.square);

                //значения для слайдера цен (c '.' для функционала)
                let sliderMinPrice = newMinPriceForHtml / 1000000;
                let sliderMaxPrice = newMaxPriceForHtml / 1000000;

                //значения для слайдера цена (с ',' для пользователя)
                let sliderMinPriceHtml = number_format(newMinPriceForHtml / 1000000,2,'.','');
                let sliderMaxPriceHtml = number_format(newMaxPriceForHtml / 1000000,2,'.','');

                document.getElementById('min_price').innerHTML = sliderMinPriceHtml;
                document.getElementById('max_price').innerHTML = sliderMaxPriceHtml;
                document.getElementById('min_floor').innerHTML = newMinFloorForHtml;
                document.getElementById('max_floor').innerHTML = newMaxFloorForHtml;
                document.getElementById('min_square').innerHTML = newMinSquareForHtml;
                document.getElementById('max_square').innerHTML = newMaxSquareForHtml;

                //Слайдер по ценам
                $("#range_slider_price").slider({
                    range: true,
                    step:0.01,
                    animate: "fast",
                    values: [+sliderMinPriceHtml, +sliderMaxPriceHtml],
                    min: +sliderMinPriceHtml,
                    max: +sliderMaxPriceHtml,
                    slide: function (event, ui) {
                        //в фильтр идёт число, умноженное на 1млн., чтобы фильтрация не ломалась
                        $('#min_price').html(number_format(ui.values[0],2,'.',''));
                        $('#max_price').html(number_format(ui.values[1],2,'.',''));
                        $('#min_price_val').val(+ui.values[0] * 1000000);
                        $('#max_price_val').val(+ui.values[1] * 1000000);

                        console.log(ui.values[0],ui.values[1])

                        refreshFilterResult();
                    },
                    change: function () {
                        /* console.log('change'); */
                    }
                });

                //Слайдер по этажам
                $("#range_slider_floor").slider({
                    range: true,
                    animate: "fast",
                    values: [newMinFloorForHtml, newMaxFloorForHtml],
                    min: +newMinFloorForHtml,
                    max: +newMaxFloorForHtml,
                    slide: function (event, ui) {
                        $('#min_floor').html(ui.values[0]);
                        $('#max_floor').html(ui.values[1]);
                        $('#min_floor_val').val(ui.values[0]);
                        $('#max_floor_val').val(ui.values[1]);

                        refreshFilterResult();
                    }
                });

                //Слайдер по площади

                $("#range_slider_square").slider({
                    range: true,
                    animate: "fast",
                    values: [newMinSquareForHtml, newMaxSquareForHtml],
                    min: +newMinSquareForHtml,
                    max: +newMaxSquareForHtml,
                    slide: function (event, ui) {
                        $('#min_square').html(number_format(ui.values[0], 0, '', ''));
                        $('#max_square').html(number_format(ui.values[1], 0, '', ''));
                        $('#min_square_val').val(ui.values[0]);
                        $('#max_square_val').val(ui.values[1]);

                        refreshFilterResult();
                    }
                });
            }

            $('.filter-showmore').on('click', function () {
                page++;

                createFlatItemListHTML();

                return false;
            });

            refreshFilterResult();

            if (FILTER_DB.length) {
                filterForRangeAfter(FILTER_DB);
            }

            createFilterResultHTML();

            initLocationTabs();
    })

    // Склонение слов по падежам в зависимости от количесва
    // Пример: declOfNum(term, ['неделя', 'недели', 'недель'])
    function declOfNum(number, titles) {
        const cases = [2, 0, 1, 1, 1, 2];
        return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
    }


//денежный формат
    function number_format(number, decimals, dec_point, thousands_sep) {
        let i, j, kw, kd, km;
        if (isNaN(decimals = Math.abs(decimals))) {
            decimals = 2;
        }
        if (dec_point == undefined) {
            dec_point = ',';
        }
        if (thousands_sep == undefined) {
            thousands_sep = '.';
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
        if ((j = i.length) > 3) {
            j = j % 3;
        } else {
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : '');
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands_sep);
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
        return km + kw + kd;
    }
}