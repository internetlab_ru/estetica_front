import Swiper from 'swiper/bundle';
import {isDesktop, isMobile} from "../helpers";
import {SVG} from "@svgdotjs/svg.js";

export function initApartmentLanding(){
    if(!$('#apartment_landing').length) return;

    const $block = $('.drawing-block')
    const $drawing = $block.find('[data-drawing]');
    let drawingWidth = $drawing.attr('data-width') || 650;
    let drawingHeight = $drawing.attr('data-height') || 650;

    let dataItems = JSON.parse($drawing.attr('data-items'));
    let drawingItems = [];

    const drawingOptions = {
        id: $drawing.attr('id'),
        image: $drawing.attr('data-image'),
        colorInSale: 'rgba(255, 54, 0, 0.8)',
        colorDisabled: 'rgba(28,34,38,0.8)',
        width: drawingWidth,
        height: drawingHeight
    }


    let draw = drawingInit();
    addDrawingItems();

    if(isDesktop()){
        $('body').removeClass('body-overflow')
    }

    $(window).on('resize',() => {

        if(isDesktop()){
            $('body').removeClass('body-overflow')
        }
    })

    new Swiper('.swiper', {
        spaceBetween: 20,
        navigation: {
            nextEl: isMobile() ? ".swiper-button-next" : ".swiper-button-prev",
            prevEl: isMobile() ? ".swiper-button-prev" : ".swiper-button-next",
        },
    });

    function drawingInit(){
        const draw = SVG().addTo(`#${drawingOptions.id}`);

        draw.viewbox(0, 0, drawingOptions.width, drawingOptions.height);

        const image = draw.image(drawingOptions.image);
        image.size('100%', '100%').move(0, 0);

        return draw;
    }

    function addDrawingItems(){
        for (let i = 0; i < dataItems.length; i++) {
            const item = dataItems[i];
            if (!item.id) continue;

            addItemSVG(item);
        }
    }

    function addItemSVG(item){
        const {
            id = null, sold = '', coords = '',
        } = item;

        drawingItems[id] = draw.polygon(coords);

        if (sold === '1') {
            drawingItems[id].fill(drawingOptions.colorDisabled);

        } else {
            drawingItems[id].fill(drawingOptions.colorInSale);
        }
    }
}