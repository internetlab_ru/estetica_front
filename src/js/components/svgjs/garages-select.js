import * as $ from 'jquery';
import {SVG} from '@svgdotjs/svg.js';
import {isDesktop} from "../helpers";

export default class GaragesSelect {
    constructor($block) {
        this.$block = $block;
        this.$drawing = $block.find('[data-drawing]');

        if (!this.$drawing.length) return false;

        this.desktop = null;

        this.items = JSON.parse(this.$drawing.attr('data-items'));
        this.garageSection = this.$drawing.attr('data-garage-section');

        this.drawingItems = [];

        this.width = this.$drawing.attr('data-width') || 650;
        this.height = this.$drawing.attr('data-height') || 650;

        this.drawingOptions = {
            id: this.$drawing.attr('id'),
            image: this.$drawing.attr('data-image'),
            colorBase: 'rgba(255, 77, 0, 0.3)',
            colorFill: 'rgba(255, 255, 255, .3)',
            colorNoFill: 'rgba(0,0,0,0)',
            colorDisabled: 'rgba(30, 36, 41, 0.88)',
            fillOptions: ['246, 184, 53', '255, 92, 48', '255, 54, 0'],
            colorSquares:{
              red : 'rgba(244,184,174,1)',
              yellow : 'rgba(250,223,168,1)',
              green : 'rgba(182,241,190,1)',
              lightBlue : 'rgba(195,238,244,1)'
            },
            width: this.width,
            height: this.height
        }

        this.init();
    }

    init() {
        this.$popupInfo = this.createPopup();
        this.$popupInfoContent = this.$popupInfo.find('.content');

        this.draw = this.drawingInit();

        this.addDrawingItems();
        this.drawingResize();

        $(window)
            .on('mousemove', (e) => {
                this.setPopupPosition(e);
            })
            .on('resize', (e) => {
                this.drawingResize();
                this.initEventsHandler();
            });

        // закрытие тултипа
        $('body').on('click', '[data-close-popup-info]', () => {
            this.hidePopup();
            return false;
        });

        window.innerWidth === 1280 || window.innerWidth === 1395 || window.innerWidth === 1366 || window.innerWidth === 1370 ? $('.body').css('overflow','auto') : '';
        window.innerWidth === 1280 || window.innerWidth === 1395 || window.innerWidth === 1366 ? $('#garages').css('overflow','auto') : '';
        $(window).on('resize',() => {
            window.innerWidth === 1280 || window.innerWidth === 1395 || window.innerWidth === 1366 || window.innerWidth === 1370 ? $('.body').css('overflow','auto') : $('.body').css('overflow','hidden');
        })

        $('body').css('background','#121212')

        const currentSection = $('.entrance-block').data('current-entrance');
        $(`a[data-section="${currentSection}"]`).addClass('active');

        // при скроле удаляем кнопку Перемещайте генплан на моб.версии
        $('.move-button-wrapper').on('touchstart ', () => {
            if ($('body').find('.move-button-wrapper').length) {
                $('body').find('.move-button-wrapper').remove();
                this.$drawing.removeClass('move-style');
            }
        });

        this.initEventsHandler();
    }

    drawingResize() {
       /* if(isDesktop()){
            this.$drawing.css({
                width: `100%`,
                height: `100%`,
                marginLeft: '',
                marginTop:''
            });
            return
        }*/

        let headerHeight = +$('#header').height();
        if (!this.isDesktop()) headerHeight = +$('#header_mobile').height();

        const kHW = 0.514;
        const kWH = 1.942;

        const windowWidth = window.innerWidth;
        const windowHeight = window.innerHeight - headerHeight;

        const kWindow = windowHeight / windowWidth;

        let widthDrawing = windowHeight * kWH;
        let heightDrawing = windowHeight;

        if (kWindow < kHW) {
            widthDrawing = windowWidth;
            heightDrawing = windowWidth * kHW;
        }

        let scale = widthDrawing / 1920 + 0.2;
        if (scale > 1) scale = 1;
        this.$block.find('.item').css({
            'transform': `translateX(-50%) translateY(-50%) scale(${scale})`
        });

        let upperBlockHeight = $('.upper-block-garages').height();

        this.$drawing.css({
            width:this.isDesktop() ? '100%': `${widthDrawing / 1.5}px`,
            height: `calc(100vh - 119px - ${upperBlockHeight}px)`,
            marginLeft: this.isDesktop() ? '':`${-widthDrawing / 2}px`,
            marginTop:this.isDesktop() ? '':`${-heightDrawing / 2}px`
        });

        if(this.$drawing.attr('data-garage-section') === '2' && !this.isDesktop()){
            this.$drawing.find('svg').css('height','85%');
            this.$drawing.find('svg').css('margin-top','15px');
        }
    }


    /**
     * Обработчик событий холста
     * для десктопа и мобильной версии разные события
     */
    initEventsHandler() {
        const desktop = this.isDesktop();
        if (desktop !== this.desktop) this.desktop = desktop;

        this.$block.off('mouseenter', '.item');
        this.$block.off('mouseleave', '.item');
        this.$block.off('click', '.item');


        this.drawingItems.forEach((item, i) => {
            const initItem = this.drawingItems[i];

            item.off('mouseenter');
            item.off('mouseleave');

            if (this.desktop) {
                item
                    .on('mouseenter', () => {
                        this.handlerMouseenter(initItem);
                    })
                    .on('mouseleave', () => {
                        this.handlerMouseleave(initItem);
                    })
                    .on('click', () => {
                        this.handlerClick(initItem);
                    });

            } else {
                item.on('click', () => {
                    this.handlerClickMobile(initItem);
                    return false;
                });
            }

        })

    }

    drawingInit() {
        const draw = SVG().addTo(`#${this.drawingOptions.id}`);

        draw.viewbox(0, 0, this.drawingOptions.width, this.drawingOptions.height);

        const image = draw.image(this.drawingOptions.image);
        image.size('100%', '100%').move(0, 0);

        return draw;
    }

    /**
     * Добавлеине навигационных элементов секций
     * @returns {boolean}
     */
    addDrawingItems() {
        for (let i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            if (!item.id) continue;

            this.addItemSVG(item);
        }
    }

    /**
     * Добавление SVG элемента на холст
     * @param {Object} item - данные о секции
     */
    addItemSVG(item = {}) {
        /*цвета для гаражей в зависимости от площади
            * 1 - красный ( > 25м)
            * 2 - желтый (20-25)
            * 3 - зеленый (17-19)
            * 4 - голубой (13-16)
            *
        */

        const {
            id = null,
            href = '',
            garageNumber = '',
            area = '',
            price = '',
            coords = '',
            sold = '',
            crmId = null
        } = item;

        this.drawingItems[id] = this.draw.group();
        this.drawingItems[id].data({id});


        if (sold === '1') {
            //создаём группу svg
            this.drawingItems[id].addClass('garage-soled');

            //добавляем полигон на svg
            let polygonchik = `<polygon points="${coords}" fill="${this.drawingOptions.colorDisabled}" data-id="${id}" data-href="${href}" data-coords="${coords}" data-name="${name}" data-area="${area}" data-price="${price}">
                <style>cursor{0:p;1:o;2:i;3:n;4:t;5:e;6:r;}</style>
                <style>position{0:r;1:e;2:l;3:a;4:t;5:i;6:v;7:e;}</style>
                <style></style>
                </polygon>`;

            this.drawingItems[id].add(polygonchik);

            //координаты группы относительно svg
            let groupBlock = $(`g[data-id="${id}"]`);
          /*  let carWidth = groupBlock[0].getBBox().width / 2;*/
            let carHeight = groupBlock[0].getBBox().height;
            let carX = groupBlock[0].getBBox().x + groupBlock[0].getBBox().width / 2 - 40;
            let carY = groupBlock[0].getBBox().y - 15;

            console.log(groupBlock[0].getBBox().width, groupBlock[0].getBBox().x)

            let rotateStyle = ''
            if(this.garageSection === '2'){
                rotateStyle = `transform= "rotate(13 ${carX + 40} ${carY + carHeight / 2})"`
            }
            //добавляем картинку машины в группу
            let car =`<image style="cursor:pointer;"${rotateStyle} width="80" height="${carHeight}" x="${carX}" y="${carY}" xlink:href="/img/garages/car.svg" />`
            this.drawingItems[id].add(car);

        } else {
            let colorFill = '';
            if (+area > 25){
                colorFill = this.drawingOptions.colorSquares.red;

            }else if(+area >= 20 && +area <= 25){
                colorFill = this.drawingOptions.colorSquares.yellow;

            }else if(+area >= 17 && +area <= 19.99){
                colorFill = this.drawingOptions.colorSquares.green;

            }else if (+area >= 13 && +area <= 16.99){
                colorFill = this.drawingOptions.colorSquares.lightBlue;
            }


            //добавляем полигон на svg
            let polygonchik = `<polygon 
                    points="${coords}" 
                    stroke="black"
                    stroke-dasharray="15"
                    stroke-opacity="1"
                    stroke-width="2"
                    fill="${colorFill}" 
                    data-id="${id}" 
                    data-href="${href}" 
                    data-coords="${coords}" 
                    data-name="${name}" 
                    data-area="${area}" 
                    data-price="${price}">
                    <style>cursor{0:p;1:o;2:i;3:n;4:t;5:e;6:r;}</style>
                    <style>position{0:r;1:e;2:l;3:a;4:t;5:i;6:v;7:e;}</style>
                </polygon>`;

            this.drawingItems[id].add(polygonchik);

            //координаты группы относительно svg
            let elem = $(`g[data-id="${id}"]`);
            let textX = elem[0].getBBox().x + elem[0].getBBox().width / 2;
            let textY = elem[0].getBBox().y + elem[0].getBBox().height / 2;


            //добавляем текст в группу
            let spanchik =`<text style="cursor:pointer;" dominant-baseline="middle" text-anchor="middle" x="${textX}" y="${textY}" fill="black" font-size="48">${garageNumber}</text>`
            let circle = `<circle style="fill:url(#toning);stroke:#010101;stroke-width:1;stroke-miterlimit:10;" cx="${textX}" cy="${textY - 4}" r="35">
    </circle>`

            this.drawingItems[id].add(spanchik);
            this.drawingItems[id].add(circle);

            if(this.isDesktop()){
                this.drawingItems[id].attr('data-effect-type','open-modal-fade-effect');
                this.drawingItems[id].attr('data-open-modal-button','reserve_modal');
            }


            if(this.isDesktop()){
                this.drawingItems[id].on('click',() => {
                    $('[data-apartment]').text(`${$('.garage-button.active').text()}, место № ${garageNumber}`);
                    $('input[name="crmId"]').val(crmId);
                })
            }


        }
        this.drawingItems[id].data({
            id,
            href,
            coords,
            name,
            sold,
            area,
            price,
            garageNumber
        });

        this.drawingItems[id].style('cursor', 'pointer');
        this.drawingItems[id].style('position', 'relative');

    }


    /**
     * Обработчик наведения на объект
     * @param {Object} item - svg.js объект
     */
    handlerMouseenter(item) {
        const sold = item.data('sold');
        if (sold == '1') {
            item.fill(this.drawingOptions.colorDisabled);
        } else {
            item.fill(`rgba(${this.drawingOptions.fillOptions[2]}, 0.6)`);
            this.setDataPopup(item);
            this.showPopup();
        }
    }

    /**
     * Обработчик ухода курсора с объекта
     * @param {Object} item - svg.js объект
     */
    handlerMouseleave(item) {
        const sold = item.data('sold');

        if (sold == '1') {
            item.fill(this.drawingOptions.colorDisabled);
        } else {
            item.fill(`rgba(255, 255, 255, 0)`);
        }

        this.hidePopup();
    }

    /**
     * Обработчик клика на объект - десктоп
     * @param {Object} item - svg.js объект
     */
    handlerClick(item) {
        /*window.location.href = item.data('href');*/

        return false;
    }

    /**
     * Обработчик клика на объект - мобильные устройства
     * @param {Object} item - svg.js объект
     */
    handlerClickMobile(item) {
        // открываем попап
        this.setDataPopup(item);
        this.showPopup();
    }

    /**
     * Определение десктопа
     * @returns {boolean}
     */
    isDesktop() {
        return (window.innerWidth > 1000);
    }

    /**
     * Позиционирование тултипа
     * @param {Object} e - объект с параметрами курсора
     */
    setPopupPosition(e) {
        const mouseX = e.clientX + document.body.scrollLeft;
        const mouseY = e.clientY + document.body.scrollTop;

        const tooltipHeight = this.$popupInfo.height() + 30;
        const tooltipWidth = this.$popupInfo.width() + 15;

        let top = mouseY - tooltipHeight / 2 + 25;
        let left = mouseX + 30;

        const style = {
            left: `${left}px`, top: `${top}px`
        };

        this.$popupInfo.css(style);
    }

    /**
     * Показ тултипа
     */
    showPopup() {
        this.$popupInfo.addClass('show');
        setTimeout(() => {
            this.$popupInfo.addClass('show-effect');
        }, 5);
    }

    /**
     * Закрытие тултипа
     */
    hidePopup() {
        if (this.desktop) {
            this.$popupInfo.removeClass('show show-effect');
            this.clearPopup();

        } else {
            this.$popupInfo.removeClass('show-effect');
            setTimeout(() => {
                this.$popupInfo.removeClass('show');
                this.clearPopup();
            }, 400);
        }
    }

    /**
     * Создание тултипа
     * @return {HTMLElement} созданный тултип
     */
    createPopup() {
        $('body').append(`
            <div id="popup_info" class="popup-info">
                <div class="content garage"></div>
            </div>
        `);

        return $('#popup_info');
    }

    /**
     * Добавление данных в тултип
     * @param {Object} item - svg.js объект
     */
    setDataPopup(item = {}) {
        const href = item.data('href') || '';
        const area = item.data('area') || '';
        const garageNumber = item.data('garageNumber') || '';
        const price = item.data('price') || '';
        const sold = item.data('sold') || '';


        const headHTML = `
            <div class="head rooms-head garage">
                <p class="text floorSection">Место № ${garageNumber}</p>
                <a href="#" class="close hide-desktop" data-close-popup-info><i class="icon icon-close popup-close"></i></a>
            </div>
        `;

        let apartmentsHTML = '';

        apartmentsHTML += `
            <div class="item floor-popup garage">
                <div class="text popUpSquare garage">Площадь: <span class="garage-popup-value">${area} м<sup>2</sup></span></div>
                <div class="text popUpPrice garage">Цена: <span class="garage-popup-value">${this.number_format(price,'','',' ')} ₽</span></div>
            </div>
        `;


        //Если пришёл флаг sold, ставим заглушку
        if (sold == '1') {

            apartmentsHTML = `
                <div class="item hide-desktop">
                    <span class="text sold-out">Данное место продано</span>
                </div>
            `;
        }

        apartmentsHTML = `<div>${apartmentsHTML}</div>`;

        let buttonHTML = `
            <div class="text-align-center hide-desktop">
                <a href="${href}" class="button orange margin-top margin-bottom-x1 no-margin-horizontal  popup-button"
                  data-effect-type="open-modal-fade-effect"
                  data-open-modal-button="reserve_modal"
                  id="open_garage_modal"
                >Продолжить</a>
            </div>
        `;

        //Если все продано убираем кнопку
        if (sold == '1') buttonHTML = ``;

        const dividerHTML = '<div class="hr"></div>'

        const popupHTML = headHTML + dividerHTML + apartmentsHTML + buttonHTML;
        this.$popupInfoContent.html(popupHTML);

        $('#open_garage_modal').on('click',() => {
            $('[data-apartment]').text(`${$('.garage-button.active').text()}, место № ${garageNumber}`)
        })
    }

    /**
     * Очистка HTML тултипа
     */
    clearPopup() {
        this.$popupInfoContent.html('');
    }

    //денежный формат
    number_format(number, decimals, dec_point, thousands_sep) {
        let i, j, kw, kd, km;
        if (isNaN(decimals = Math.abs(decimals))) {
            decimals = 2;
        }
        if (dec_point == undefined) {
            dec_point = ',';
        }
        if (thousands_sep == undefined) {
            thousands_sep = '.';
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
        if ((j = i.length) > 3) {
            j = j % 3;
        } else {
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : '');
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands_sep);
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
        return km + kw + kd;
    }
}