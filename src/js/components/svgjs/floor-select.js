import * as $ from 'jquery';
import { SVG } from '@svgdotjs/svg.js';
import {isDesktop} from "../helpers";

export default class FloorSelect {
    constructor($block) {
        this.$block = $block;
        this.$drawingBlock = $block.find('[data-drawing_block]');

        this.$drawing = $block.find('[data-drawing]');


        this.$filter = $block.find('[data-room-filter]');
        this.$sectionFilter = $block.find('.tabs-switchers');
        this.$sectionFilterItems = $block.find('[data-tab]');
        this.$bigPicture = $block.find('#big-picture');

        if (!this.$drawing.length) return false;

        this.desktop = null;

        this.items = JSON.parse(this.$drawing.attr('data-items'));

        this.drawingItems = [];
        this.drawingPaths = [];

        this.roomsFilters = [];
        this.sectionFilter = [];

        this.drawingOptions = {
            id: this.$drawing.attr('id'),
            image: this.$drawing.attr('data-image'),
            balloon: this.$drawing.attr('data-balloon'),
            colorBase: 'rgba(255, 0, 0, 0)',
            pathFill: 'rgba(0, 0, 0, 0.5)',
            colorFill: 'rgba(255, 0, 0, 0)',
            colorInSale: 'rgba(255, 54, 0, 0.2)',
            colorInSaleHover: 'rgba(255, 54, 0, 0.6)',
            colorDisabled: 'rgba(133, 133, 133, 0.2)',
            strokeBase: { color: '#fff', opacity: 1, width: 2, dasharray: '7' },
            strokeFill: { color: '#fff', opacity: 0, width: 1 },
            strokeDisabled: { color: '#fff', opacity: 0, width: 3 },
            width: 2560,
            height: 1250
        }


        this.init();
    }

    init() {
        //включаем горизонтальный скролл как вертикальный на выборе этажа
        $('[data-drawing_block]').bind('mousewheel',function(e) {
            e.preventDefault();
            const $drawingBlock = $('[data-drawing_block]');
            let currentScroll = $drawingBlock.scrollLeft();

            if(+e.originalEvent.deltaY === -100){
                $drawingBlock.scrollLeft(currentScroll - 50);

            }else if(+e.originalEvent.deltaY === 100){
                $drawingBlock.scrollLeft(currentScroll + 50);
            }
        });


        this.$popupInfo = this.createPopup();
        this.$popupInfoContent = this.$popupInfo.find('.content');

        this.draw = this.drawingInit();

        this.addDrawingItems();

        this.drawingResize();

        this.repositioningRoomsFilter();

        $(window)
            .on('mousemove', (e) => {
                this.setPopupPosition(e);
            })
            .on('resize', (e) => {
                this.drawingResize();

                this.initEventsHandler();
                this.repositioningRoomsFilter();
            });

        // закрытие тултипа
        $('body').on('click', '[data-close-popup-info]', () => {
            this.hidePopup();

            return false;
        });


        if (this.$filter.find('.filter-item.active').length !== 0) {
            this.roomsFilters = [1,2,3];
            this.filterRooms();
        }

        this.$filter.on('click', '.filter-item', (e) => {
            const $this = e.currentTarget;
            const room = $this.dataset['room'];

            $this.classList.toggle('active');

            if(this.$filter.find('.filter-item.active').length === 0){
                this.$filter.find('.filter-item').each((i, filter) => {
                    $(filter).addClass('active');
                })
                this.roomsFilters = [1,2,3];
            }

           else{
                if (room === '0') {
                    $($this).siblings().removeClass('active')
                    this.roomsFilters = [];
                } else {
                    $($this).siblings('[data-room="0"]').removeClass('active');
                    if (this.roomsFilters.includes(0)) { this.roomsFilters.splice(0) }
                }

                if ($this.classList.contains('active')) {
                    this.roomsFilters.push(+room)
                } else {
                    this.roomsFilters.splice(this.roomsFilters.indexOf(+room), 1)
                }
            }

            this.filterRooms();
            return false;
        });

        // при скроле удаляем кнопку Перемещайте генплан на моб.версии
        this.$drawingBlock.on('scroll', () => {
            if (this.$block.find('.move-button').length) {
                this.$block.find('.move-button').remove();
                this.$drawing.removeClass('move-style');
                $('.filter-item').each((_,filter) => {
                    $(filter).addClass('online');
                })
            }
        });

        this.initEventsHandler();
    }

    repositioningRoomsFilter() {
        const $absoluteWrapper = $('.absolute-wrapper .container');
        const absoluteWrapperLeft = $absoluteWrapper.offset().left;
        this.$filter.css({
            right: absoluteWrapperLeft
        })
    }

    /**
     * Обработчик событий холста
     * для десктопа и мобильной версии разные события
     */
    initEventsHandler() {
        const desktop = this.isDesktop();
        if (desktop !== this.desktop) this.desktop = desktop;

        this.$block.off('mouseenter', '.item');
        this.$block.off('mouseleave', '.item');
        this.$block.off('click', '.item');


        this.drawingItems.forEach((item) => {
            item.off('click');
            item.off('mouseenter');
            item.off('mouseleave');

            if (this.desktop) {

                item
                    .on('mouseenter', () => {

                        this.handlerMouseenter(item);
                    })
                    .on('mouseleave', () => {
                        this.handlerMouseleave(item);
                    })
                    .on('click', () => {
                        this.handlerClick(item);
                    });

            } else {
                item.on('click', () => {
                    this.handlerClickMobile(item);
                    return false;
                });
            }
        })
    }

    drawingInit() {
        const draw = SVG().addTo(`#${this.drawingOptions.id}`);

        draw.viewbox(0, 0, this.drawingOptions.width, this.drawingOptions.height);

        const image = draw.image(this.drawingOptions.image);
        image.size('100%', '100%').move(0, 0);

        const balloon = draw.image(this.drawingOptions.balloon)
        balloon.size('100%', '100%').move(0, 0);

        image.id('drawingImg');

        return draw;
    }


    drawingResize() {
        let headerHeight = +$('#header').height();
        if (!this.isDesktop()) headerHeight = +$('#header_mobile').height();

        const kHW = 0.48828;
        const kWH = 2.048;

        const windowWidth = window.innerWidth;
        const windowHeight = window.innerHeight - headerHeight;

        const kWindow = windowHeight / windowWidth;

        let widthDrawing = windowHeight * kWH;
        let heightDrawing = windowHeight;

        if (kWindow < kHW) {
            widthDrawing = windowWidth;
            heightDrawing = windowWidth * kHW;
        }

        let scale = widthDrawing / 1920 + 0.2;
        if (scale > 1) scale = 1;
        this.$block.find('.item').css({
            'transform': `translateX(-50%) translateY(-50%) scale(${scale})`
        });

        //ipad air
        function checkIpadLandscape (){
            if(window.innerWidth === 1180 || window.innerWidth === 1024){
                return `${-window.innerWidth / 2}px`
            }
            return false;
        }

        this.$drawing.css({
            width: `${widthDrawing}px`,
            height: `${!isDesktop() ? heightDrawing + 40 : heightDrawing}px`,
            marginLeft: checkIpadLandscape() || `${-widthDrawing / 2}px`,
            marginTop: `${-heightDrawing / 2}px`
        });
    }

    /**
     * Добавлеине навигационных элементов секций
     * @returns {boolean}
     */
    addDrawingItems() {
        for (let i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            if (!item.id) continue;

            this.addItemSVG(item);
        }
    }


    /**
     * Добавление SVG элемента на холст
     * @param {Object} item - данные о секции
     */
    addItemSVG(item = {}) {
        const {
            id = null,
            href = '',
            name = '',
            nameType = '',
            sold = '',
            deadline = '',
            sectionName = '',
            view = '',
            coords = '',
            rooms = [],
            apartments = [],
            dateEnd = ''
        } = item;

        this.drawingItems[id] = this.draw.polygon(coords);
        this.drawingPaths[id] = this.draw.path(view);


        if (sold == '1') {
            /*this.drawingItems[id].fill(this.drawingOptions.colorDisabled);*/
            this.drawingItems[id].fill(this.drawingOptions.colorFill);
            this.drawingItems[id].stroke(this.drawingOptions.strokeDisabled);
        } else {

            /*this.drawingItems[id].fill(this.drawingOptions.colorInSale);*/
            this.drawingItems[id].fill(this.drawingOptions.colorInSale);
            this.drawingItems[id].stroke(this.drawingOptions.strokeDisabled);
        }

        this.drawingItems[id].style('cursor', 'pointer');
        this.drawingItems[id].data({
            id,
            href,
            coords,
            name,
            nameType,
            sold,
            deadline,
            sectionName,
            rooms,
            apartments,
            dateEnd
        });
    }

    /**
     * Фильтрация квартир
     */
    filterRooms() {
        for (let key in this.drawingItems) {
            const obj = this.drawingItems[key];
            const rooms = obj.data('rooms') || [];
            if (rooms.length) {
                rooms.sort((a, b) => +a > +b ? 1 : -1);
            }

            if (this.roomsFilters.length) {
                if (this.roomsFilters.includes(0)) {
                    obj.show();
                } else {
                    //Сортируем, чтобы не было багов
                    this.roomsFilters.sort((a, b) => +a > +b ? 1 : -1);
                    console.log(this.roomsFilters)
                    for (let i = 0; i < this.roomsFilters.length; i++) {
                        if (rooms.length) {
                            if (rooms.includes(String(this.roomsFilters[i]))) {
                                obj.show();
                                break;
                            } else {
                                obj.hide();
                            }
                        }
                    }
                }

            } else {
                obj.show();
            }
        }
    }

    /**
     * Обработчик наведения на объект
     * @param {Object} item - svg.js объект
     */
    handlerMouseenter(item) {
        const sold = item.data('sold');
        const id = item.data('id');

        if (sold == '1') {
            item.fill(this.drawingOptions.colorFill);
            /*item.fill(this.drawingOptions.colorDisabled);*/
            item.stroke(this.drawingOptions.strokeDisabled);
        } else {
            item.fill(this.drawingOptions.colorInSaleHover);
            this.drawingPaths[id].attr('style', 'visibility:visible');
            item.stroke(this.drawingOptions.strokeFill);
        }


        this.setDataPopup(item);
        this.showPopup();
    }

    /**
     * Обработчик ухода курсора с объекта
     * @param {Object} item - svg.js объект
     */
    handlerMouseleave(item) {
        const sold = item.data('sold');
        const id = item.data('id');

        if (sold == '1') {
            /*item.fill(this.drawingOptions.colorDisabled);*/
            item.fill(this.drawingOptions.colorFill);
            item.stroke(this.drawingOptions.strokeDisabled);
        } else {
            this.drawingPaths[id].attr({ 'style': 'visability:hidden' });
            item.fill(this.drawingOptions.colorInSale);
        }

        this.hidePopup();
    }

    /**
     * Обработчик клика на объект - десктоп
     * @param {Object} item - svg.js объект
     */
    handlerClick(item) {
        window.location.href = item.data('href');
        return false;
    }

    /**
     * Обработчик клика на объект - мобильные устройства
     * @param {Object} item - svg.js объект
     */
    handlerClickMobile(item) {
        // открываем попап
        this.setDataPopup(item);
        this.showPopup();
    }

    /**
     * Определение десктопа
     * @returns {boolean}
     */
    isDesktop() {
        return (window.innerWidth > 1000);
    }

    /**
     * Позиционирование тултипа
     * @param {Object} e - объект с параметрами курсора
     */
    setPopupPosition(e) {
        const mouseX = e.clientX + document.body.scrollLeft;
        const mouseY = e.clientY + document.body.scrollTop;

        const tooltipHeight = this.$popupInfo.height() + 30;
        const tooltipWidth = this.$popupInfo.width() + 15;

        let top = mouseY - tooltipHeight / 2 + 25;
        let left = mouseX + 30;


        const style = {
            left: `${left}px`,
            top: `${top}px`
        };

        this.$popupInfo.css(style);
    }

    /**
     * Показ тултипа
     */
    showPopup() {
        this.$popupInfo.addClass('show');
        setTimeout(() => {
            this.$popupInfo.addClass('show-effect');
        }, 5);
    }

    /**
     * Закрытие тултипа
     */
    hidePopup() {
        if (this.desktop) {
            this.$popupInfo.removeClass('show show-effect');
            this.clearPopup();

        } else {
            this.$popupInfo.removeClass('show-effect');
            setTimeout(() => {
                this.$popupInfo.removeClass('show');
                this.clearPopup();
            }, 400);
        }
    }

    /**
     * Создание тултипа
     * @return {HTMLElement} созданный тултип
     */
    createPopup() {
        $('body').append(`
            <div id="popup_info" class="popup-info">
                <div class="content"></div>
            </div>
        `);

        return $('#popup_info');
    }

    /**
     * Добавление данных в тултип
     * @param {Object} item - svg.js объект
     */
    setDataPopup(item = {}) {
        const href = item.data('href') || '';
        const name = item.data('name') || '';
        const sectionName = item.data('sectionName') || '';
        const nameType = item.data('nameType') || '';
        const apartments = item.data('apartments') || [];
        const sold = item.data('sold') || '0';
        const dateEnd = item.data('dateEnd') || '';


        const headHTML = `
            <div class="head rooms-head">
                <p class="text floorSection">${name} этаж, секция ${sectionName}</p>
                <p class="text cellPopHeadText">Сдача ${dateEnd}</p>
                <a href="#" class="close hide-desktop" data-close-popup-info><i class="icon icon-close popup-close"></i></a>
            </div>
        `;

        let apartmentsHTML = '';

        for (let i = 0; i < apartments.length; i++) {
            const {
                area = '',
                count = '',
                price = '',
                rooms = ''
            } = apartments[i];

            let disabledClass = '';

            if (this.roomsFilters.length) {
                if (!this.roomsFilters.includes(+rooms)) {
                    disabledClass = 'disabled';
                }

                if (this.roomsFilters.includes(0)) {
                    disabledClass = '';
                }

            }

            const roomName = ['Ст', '1к', '2к', '3к', '4к', '5к'];

            apartmentsHTML += `
                <div class="item ${disabledClass}">
                    <div class="rooms rooms-${rooms}">
                        ${roomName[rooms]}
                    </div>
                    <div class="text popUpSquare">от  ${area} м<sup>2</sup></div>
                    <div class="text popUpPrice">от ${price} млн. </div>
                </div>
            `;
        }

        //Если пришёл флаг sold, ставим заглушку
        if (sold == '1') {
            apartmentsHTML = `
                <div class="item">
                    <span class="text sold-out">Все квартиры проданы</span>
                </div>
            `;
        }

        apartmentsHTML = `<div>${apartmentsHTML}</div>`;

        let buttonHTML = `
            <div class="text-align-center hide-desktop">
                <a href="${href}" class="button orange margin-top margin-bottom-x2 no-margin-horizontal  popup-button">Продолжить</a>
            </div>
        `;

        //Если все продано убираем кнопку
        if (sold == '1') buttonHTML = ``;

        const dividerHTML = '<div class="hr"></div>'

        const popupHTML = headHTML + dividerHTML + apartmentsHTML + buttonHTML;
        this.$popupInfoContent.html(popupHTML);
    }

    /**
     * Очистка HTML тултипа
     */
    clearPopup() {
        this.$popupInfoContent.html('');
    }
}
