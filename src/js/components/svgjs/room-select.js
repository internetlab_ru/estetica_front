import * as $ from 'jquery';
import {SVG} from '@svgdotjs/svg.js';
// import { declOfNum, numberFormat } from '../helpers'

export default class RoomSelect {
    constructor($block) {
        this.$block = $block;
        this.$drawing = $block.find('[data-drawing]');
        this.$locationDrawing = $block.find('#map_drawing');

        if (!this.$drawing.length) return false;

        this.desktop = null;

        this.items = JSON.parse(this.$drawing.attr('data-items'));
        this.locatonItems = JSON.parse(this.$locationDrawing.attr('data-items'));

        this.drawingItems = [];
        this.drawingLocationItems = [];

        this.width = this.$drawing.attr('data-width') || 650;
        this.height = this.$drawing.attr('data-height') || 650;

        this.drawingOptions = {
            id: this.$drawing.attr('id'),
            image: this.$drawing.attr('data-image'),
            colorBase: 'rgba(255, 77, 0, 0.3)',
            colorFill: 'rgba(255, 255, 255, .3)',
            colorNoFill: 'rgba(0,0,0,0)',
            colorDisabled: 'rgba(30, 36, 41, 0.88)',
            fillOptions: ['246, 184, 53', '255, 92, 48', '255, 54, 0'],
            width: this.width,
            height: this.height
        }

        this.init();
    }

    init() {
        this.$popupInfo = this.createPopup();
        this.$popupInfoContent = this.$popupInfo.find('.content');

        this.draw = this.drawingInit();
        this.drawLocation = this.locationDrawingInit();

        this.addDrawingItems();
        this.addLocatonDrawingItems();
        $(window)
            .on('mousemove', (e) => {
                this.setPopupPosition(e);
            })
            .on('resize', (e) => {
                this.initEventsHandler();
            });

        // закрытие тултипа
        $('body').on('click', '[data-close-popup-info]', () => {
            this.hidePopup();
            return false;
        });

        const currentSection = $('.entrance-block').data('current-entrance');
        $(`a[data-section="${currentSection}"]`).addClass('active');

        this.initEventsHandler();
    }

    locationDrawingInit() {
        const draw = SVG().addTo('#map_drawing');
        draw.viewbox(0, 0, 297, 324);
        const image = draw.image(this.$locationDrawing.data('image'));

        image.size('100%', '100%').move(0, 36);

        return draw;
    }


    /**
     * Обработчик событий холста
     * для десктопа и мобильной версии разные события
     */
    initEventsHandler() {
        const desktop = this.isDesktop();
        if (desktop !== this.desktop) this.desktop = desktop;

        this.$block.off('mouseenter', '.item');
        this.$block.off('mouseleave', '.item');
        this.$block.off('click', '.item');


        this.drawingItems.forEach((item, i) => {
            const initItem = this.drawingItems[i];

            item.off('click');
            item.off('mouseenter');
            item.off('mouseleave');

            if (this.desktop) {
                item
                    .on('mouseenter', () => {
                        this.handlerMouseenter(initItem);
                    })
                    .on('mouseleave', () => {
                        this.handlerMouseleave(initItem);
                    })
                    .on('click', () => {
                        this.handlerClick(initItem);
                    });

            } else {
                item.on('click', () => {
                    this.handlerClickMobile(initItem);
                    return false;
                });
            }

        })

        this.drawingLocationItems.forEach((item, i) => {
            item.off('click');
            item.off('mouseenter');
            item.off('mouseleave');

            item
                .on('click', () => {
                    this.handlerClick(item);
                    return false;
                });
        })

    }

    drawingInit() {
        const draw = SVG().addTo(`#${this.drawingOptions.id}`);

        draw.viewbox(0, 0, this.drawingOptions.width, this.drawingOptions.height);

        const image = draw.image(this.drawingOptions.image);
        image.size('100%', '100%').move(0, 0);

        return draw;
    }

    /**
     * Добавлеине навигационных элементов секций
     * @returns {boolean}
     */
    addDrawingItems() {
        for (let i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            if (!item.id) continue;

            this.addItemSVG(item);
        }
    }

    addLocatonDrawingItems() {
        let currentFloor = 0;

        for (let i = 0; i < this.locatonItems.length; i++) {
            const item = this.locatonItems[i];
            if(item.current === '1') currentFloor = +item.floor;

            this.addLocationItemSVG(item);
        }

        //позиционирование текста рядом с картинкой дома
        $('.current-location').text(`${currentFloor} ЭТАЖ`);


        this.positioningMapText(currentFloor);
        this.initResizeLocation(currentFloor);
    }

    initResizeLocation(currentFloor){
        $(window).on('resize',() => {
            this.positioningMapText(currentFloor);
        })
    }

    positioningMapText(currentFloor){
        const $locationTextBlock = $('.current-location');
        const $mapDraw = $('#map_drawing');

        let mapDrawHeight = $mapDraw.outerHeight();
        let locationTextBlockHeight = $locationTextBlock.outerHeight();
        $locationTextBlock.css('bottom',`${(mapDrawHeight - mapDrawHeight / 9) / 9 * currentFloor - locationTextBlockHeight}px`);
    }

    addLocationItemSVG(item = {}) {
        const {
            id = null, href = '', coords = '',floor = '', current = ''
        } = item;

        //этаж для моб версии локации
        const $floor = $(`.floor[data-floor="${floor}"]`);

        this.drawingLocationItems[id] = this.drawLocation.polygon(coords);
        this.drawingLocationItems[id].fill('rgba(255, 54, 0,0)');

        $floor.removeClass('disabled').addClass('available').attr('href',href);

        if(current === '1'){
            this.drawingLocationItems[id].fill('rgba(255, 54, 0,0.5)');
            $floor.removeClass('disabled available').addClass('current').attr('href','');
        }

        this.drawingLocationItems[id].addClass('pointer')
        this.drawingLocationItems[id].data({
            id, href, coords,current
        });
    }

    /**
     * Добавление SVG элемента на холст
     * @param {Object} item - данные о секции
     */
    addItemSVG(item = {}) {
        const {
            id = null, href = '', name = '', sold = '', area = '', price = '', rooms = '', coords = '', cellNumber = '',
            long = ''
        } = item;


        if (sold == '1') {
            //создаём группу svg
            this.drawingItems[id] = this.draw.group();
            this.drawingItems[id].data({id});
            this.drawingItems[id].addClass('soled')

            //добавляем полигон на svg
            let polygonchik = `<polygon points="${coords}" fill="${this.drawingOptions.colorDisabled}" data-id="${id}" data-href="${href}" data-coords="${coords}" data-name="${name}" data-area="${area}" data-price="${price}" data-rooms="${rooms}" data-cellNumber="${cellNumber}">
                <style>cursor{0:p;1:o;2:i;3:n;4:t;5:e;6:r;}</style>
                <style>position{0:r;1:e;2:l;3:a;4:t;5:i;6:v;7:e;}</style>
                </polygon>`;

            this.drawingItems[id].add(polygonchik);

            //координаты группы относительно svg
            let elem = $(`g[data-id="${id}"]`);
            let textX = elem[0].getBBox().x + elem[0].getBBox().width / 2;
            let textY = elem[0].getBBox().y + elem[0].getBBox().height / 2;
            //если большая квартира - уменьшаем/увеличиваем координаты для текста, чтобы он был выше в зависимости от положения большой квартиры
            if(long === '1'){
                 textY = elem[0].getBBox().y + elem[0].getBBox().height / 2 - 300;

            }else if(long === '-1'){
                textY = elem[0].getBBox().y + elem[0].getBBox().height / 2 + 300;
            }


            //добавляем текст в группу
            let spanchik =`<text style="cursor:pointer;" dominant-baseline="middle" text-anchor="middle" x="${textX}" y="${textY}" fill="rgba(255, 255, 255, 0.6)" font-size="50">ПРОДАНА</text>`
            this.drawingItems[id].add(spanchik);

            this.drawingItems[id].data({
                id, href, coords, name, sold, area, price, rooms, cellNumber
            });

        } else {
            this.drawingItems[id] = this.draw.polygon(coords);
            this.drawingItems[id].fill(`rgba(255, 255, 255, .0)`);
            this.drawingItems[id].data({
                id, href, coords, name, sold, area, price, rooms, cellNumber
            });
        }

        this.drawingItems[id].style('cursor', 'pointer');
        this.drawingItems[id].style('position', 'relative');

    }


    /**
     * Обработчик наведения на объект
     * @param {Object} item - svg.js объект
     */
    handlerMouseenter(item) {
        const sold = item.data('sold');
        if (sold == '1') {
            item.fill(this.drawingOptions.colorDisabled);
        } else {
            item.fill(`rgba(${this.drawingOptions.fillOptions[2]}, 0.6)`);
            this.setDataPopup(item);
            this.showPopup();
        }
    }

    /**
     * Обработчик ухода курсора с объекта
     * @param {Object} item - svg.js объект
     */
    handlerMouseleave(item) {
        const sold = item.data('sold');

        if (sold == '1') {
            item.fill(this.drawingOptions.colorDisabled);
        } else {
            item.fill(`rgba(255, 255, 255, 0)`);
        }

        this.hidePopup();
    }

    /**
     * Обработчик клика на объект - десктоп
     * @param {Object} item - svg.js объект
     */
    handlerClick(item) {
        if(item.data('sold') !== 1){
            window.location.href = item.data('href');
        }

        return false;
    }

    /**
     * Обработчик клика на объект - мобильные устройства
     * @param {Object} item - svg.js объект
     */
    handlerClickMobile(item) {
        // открываем попап
        this.setDataPopup(item);
        this.showPopup();
    }

    /**
     * Определение десктопа
     * @returns {boolean}
     */
    isDesktop() {
        return (window.innerWidth > 1000);
    }

    /**
     * Позиционирование тултипа
     * @param {Object} e - объект с параметрами курсора
     */
    setPopupPosition(e) {
        const mouseX = e.clientX + document.body.scrollLeft;
        const mouseY = e.clientY + document.body.scrollTop;

        const tooltipHeight = this.$popupInfo.height() + 30;
        const tooltipWidth = this.$popupInfo.width() + 15;

        let top = mouseY - tooltipHeight / 2 + 25;
        let left = mouseX + 30;

        const style = {
            left: `${left}px`, top: `${top}px`
        };

        this.$popupInfo.css(style);
    }

    /**
     * Показ тултипа
     */
    showPopup() {
        this.$popupInfo.addClass('show');
        setTimeout(() => {
            this.$popupInfo.addClass('show-effect');
        }, 5);
    }

    /**
     * Закрытие тултипа
     */
    hidePopup() {
        if (this.desktop) {
            this.$popupInfo.removeClass('show show-effect');
            this.clearPopup();

        } else {
            this.$popupInfo.removeClass('show-effect');
            setTimeout(() => {
                this.$popupInfo.removeClass('show');
                this.clearPopup();
            }, 400);
        }
    }

    /**
     * Создание тултипа
     * @return {HTMLElement} созданный тултип
     */
    createPopup() {
        $('body').append(`
            <div id="popup_info" class="popup-info">
                <div class="content"></div>
            </div>
        `);

        return $('#popup_info');
    }

    /**
     * Добавление данных в тултип
     * @param {Object} item - svg.js объект
     */
    setDataPopup(item = {}) {
        const href = item.data('href') || '';
        const area = item.data('area') || '';
        const cellNumber = item.data('cellNumber') || '';
        const rooms = item.data('rooms') || '';
        const price = item.data('price') || '';
        const sold = item.data('sold') || '';


        const headHTML = `
            <div class="head rooms-head">
                <p class="text floorSection">${rooms}-комнатная № ${cellNumber}</p>
                <a href="#" class="close hide-desktop" data-close-popup-info><i class="icon icon-close popup-close"></i></a>
            </div>
        `;

        let apartmentsHTML = '';

        apartmentsHTML += `
            <div class="item floor-popup">
                <div class="text cellNumber">Комнат: <span>${rooms}</span></div>
                <div class="text popUpSquare">Площадь: <span>${area} м<sup>2</sup></span></div>
                <div class="text popUpPrice">Цена: <span>${this.number_format(price,'','',' ')} ₽</span></div>
            </div>
        `;


        //Если пришёл флаг sold, ставим заглушку
        if (sold == '1') {
            apartmentsHTML = `
                <div class="item hide-desktop">
                    <span class="text sold-out">Все квартиры проданы</span>
                </div>
            `;
        }

        apartmentsHTML = `<div>${apartmentsHTML}</div>`;

        let buttonHTML = `
            <div class="text-align-center hide-desktop">
                <a href="${href}" class="button orange margin-top margin-bottom-x2 no-margin-horizontal  popup-button">Продолжить</a>
            </div>
        `;

        //Если все продано убираем кнопку
        if (sold == '1') buttonHTML = ``;

        const dividerHTML = '<div class="hr"></div>'

        const popupHTML = headHTML + dividerHTML + apartmentsHTML + buttonHTML;
        this.$popupInfoContent.html(popupHTML);
    }

    /**
     * Очистка HTML тултипа
     */
    clearPopup() {
        this.$popupInfoContent.html('');
    }

    //денежный формат
     number_format(number, decimals, dec_point, thousands_sep) {
        let i, j, kw, kd, km;
        if (isNaN(decimals = Math.abs(decimals))) {
            decimals = 2;
        }
        if (dec_point == undefined) {
            dec_point = ',';
        }
        if (thousands_sep == undefined) {
            thousands_sep = '.';
        }
        i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
        if ((j = i.length) > 3) {
            j = j % 3;
        } else {
            j = 0;
        }
        km = (j ? i.substr(0, j) + thousands_sep : '');
        kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands_sep);
        kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
        return km + kw + kd;
    }
}