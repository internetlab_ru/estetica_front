import * as $ from "jquery";

export function isMobile() {
    return screen.availWidth < 1000;
}

export function isTablet() {
    return screen.availWidth > 760;
}

export function isDesktop() {
    return screen.availWidth > 1000;
}

// custom placeholders
export function initPlaceholdersNew() {
    $('.placeholder').each(function () {
        if ($(this).hasClass('ready')) return;

        var $this = $(this);

        var $field = $this.parents('.field');
        var plh = $this.data('placeholder');
        var val = $.trim($this.val());
        if ((val == '' || val == plh) && plh != '' && plh != undefined) {
            $field.addClass('empty');

        } else {
            $field.removeClass('empty');
        }

        $field.prepend('<span class="label">' + plh + '</span>');

        $(this).addClass('ready');

        $(this)
            .on('focus', function () {
                var $this = $(this),
                    $field = $this.parents('.field'),
                    plh = $this.attr('data-placeholder'),
                    val = $.trim($this.val());

                if ($this.prop('readonly')) return false;

                if (val == '' || val == plh) {
                    $field.removeClass('empty');
                }
            })
            .on('blur', function () {
                var $this = $(this),
                    $field = $this.parents('.field'),
                    plh = $this.attr('data-placeholder'),
                    val = $.trim($this.val());

                if (val == '' || val == plh) {
                    $field.removeClass('error success').addClass('empty');

                } else {
                    $field.removeClass('empty');
                }
            });
    });

    $('.placeholder-select').each(function () {
        if ($(this).hasClass('ready')) return;

        var $this = $(this),
            $field = $this.parents('.field'),
            plh = $this.attr('data-placeholder'),
            val = $.trim($this.val());

        if ((val == '' || val == plh) && plh != '' && plh != undefined) {
            $field.addClass('empty');

        } else {
            $field.removeClass('empty');
        }

        $field.find('.selectwrap').prepend('<span class="label">' + plh + '</span>');

        $this.addClass('ready');

        $this
            .on('focus', function () {
                var $this = $(this),
                    $field = $this.parents('.field');
                plh = $this.attr('data-placeholder'),
                    val = $this.val();

                if ($this.prop('readonly')) return false;

                if (val == '' || val == plh) {
                    $field.removeClass('empty');
                }
            })
            .on('blur', function () {
                var $this = $(this);
                setTimeout(function () {
                    var $field = $this.parents('.field'),
                        val = $this.val(),
                        plh = $this.attr('data-placeholder');

                    if (!val || val == plh) {
                        $field.removeClass('error success').addClass('empty');

                    } else {
                        $field.removeClass('empty');
                    }
                }, 150);
            });
    });
}
