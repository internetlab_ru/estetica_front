import * as $ from 'jquery';
import 'jquery-ui';
import 'jquery-form-styler';
import 'jquery-ui-touch-punch';
import {MobileMenu} from "./components/mobile-menu";
import { ModalWindowFullScreen } from './components/modal-window-fullscreen';
import {
    initMaskedInput,
    initPlaceholders
} from './components/form';
import { InitReserveForm } from "./components/reserve-form";
import {initApartmentLanding} from "./components/apartment";
import {isDesktop, isMobile} from "./components/helpers";
import {initFilterLanding} from "./components/filter";
import FloorSelect from "./components/svgjs/floor-select";
import RoomSelect from "./components/svgjs/room-select";
import GaragesSelect from "./components/svgjs/garages-select";

$(function () {
    initScripts();
})

// инициализация скриптов
function initScripts(){
    updateViewportHeight();

    const $body = $('body');

    if ($('#room_select').length){
        $body.css('overflow','auto');
        $body.css('background-color','#121212');
    }

    if($('#apartment_landing').length && isDesktop() ){
        $body.addClass('body-overflow');
        $body.addClass('black-theme');
    }

    $(window).on('resize', () => {
        updateViewportHeight();

        if($('#apartment_landing').length && isDesktop() ){
            $body.addClass('body-overflow');

        }else if($('#apartment_landing').length && isMobile()){
            $body.removeClass('body-overflow');
        }
    });

    const mobMenu = new MobileMenu();

    $body.on('click', '.burger', ()=>{
        mobMenu.toggle();
        $('#header_mobile').toggleClass('menu-opened');
        $('.body').toggleClass('menu-opened')
        $('.burger').css('display','none')
        return false;
    });

    $body.on('click', '[data-close-mobile-menu]', () =>{
        mobMenu.toggle();
        $('#header_mobile').toggleClass('menu-opened');
        $('.body').toggleClass('menu-opened')
        $('.burger').css('display','block')
        return false;
    });

    $body.on('click','.backcall-trigger', () =>{
        $('.modal-caption-with-close').find('.modal-header-room').addClass('hide');
    });

    $body.on('click','.consult',() =>{
        $('.modal-caption-with-close').find('.modal-header-room').removeClass('hide');
    })

    //выбор этажа
    new FloorSelect($('#rc_floors'));

    //выбор квартиры
    new RoomSelect($('#rc_floor'));

    //выбор места на паркинге
    new GaragesSelect($('#rc_garages'));


    // модальные окна фулскрин
    const modalWindow = new ModalWindowFullScreen();

    //Форма заявки на бронирование
    new InitReserveForm();

    // Инициализация плейсхолдеров/масок
    initMaskedInput();
    initPlaceholders();

    //страница просмотра квартиры
    initApartmentLanding();

    //страница фильтра
    initFilterLanding();

    //hover на меню в хедере
    if(isDesktop()){
        $('.dropdown-trigger').hover(onDropdown,offDropdown);
    }
}

const updateViewportHeight = () => {
    if ($(document).width() < 1000) {
        document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 64}px`);
    } else {
        document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 96}px`);
    }
};

const onDropdown = () => {
    $('.dropdown').addClass('active');
}

const offDropdown = () => {
    $('.dropdown').removeClass('active');
}